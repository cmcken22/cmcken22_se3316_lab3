<html>
    <head>
        
        <meta charset="us-ansi">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Lab3 SE3316A</title>
    
        
        <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  
        
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Calling jquery first -->
        <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
          
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
          
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.min.js"></script>
          
        <!-- Custom styles for this template -->
        <link href="carouselStyling.css" rel="stylesheet">
        <link href="Lab3Styles.css" rel="stylesheet">
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
        
        <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
                $('.workCarousel').carousel({
                    interval: 5000
                })
            });    
            $(document).ready(function(){
                $('.dropdown-toggle').dropdown()
            });
        </script>
  
        
    </head>
    
    <body>
        
    <?php 
    
    $ID = 3;
    $title = 4;
    $description = 5;
    $year = 6;
    $width = 7; 
    $height = 8;
    $genre = 9;
    $gallery = 10;
    $price = 11;
    $URL = 12;
    
    $paintingsFile = file("data-files/paintings.txt");
    
    $paintings = array();
    
    foreach($paintingsFile as $data) {
      $info = explode("~", $data);
      //$info is now an array of data
      $paintings[] = $info; //therefore by putting $info into $paintings it makes $paintings a double array 
      //$paintings[row][column]
    }
    
    ?>
        <header>
            <div id="topHeaderRow">
                <div class="container">
                    <nav class="navbar navbar-inverse " role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                        </div>
        
                        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                            </ul>
                        </div>  <!-- end .collpase --> 
                    </nav>  <!-- end .navbar --> 
                </div>  <!-- end .container --> 
            </div>  <!-- end #topHeaderRow --> 
           
            <div id="logoRow">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Art Store</h1> 
                        </div>
                    
                    <div class="col-md-4">
                        <form class="form-inline" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="search">Search</label>
                                <input class="form-control" placeholder="Search" name="search" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                 </span>
                            </div>
                       </form> 
                    </div>   <!-- end .navbar --> 
                    </div>   <!-- end .row -->        
                </div>  <!-- end .container --> 
            </div>  <!-- end #logoRow --> 
           
            <div id="mainNavigationRow">
                <div class="container">
                    
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li class="active"><a href="work.php">Art Works</a></li>
                            <li><a href="artists.php">Artists</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Special 1</a></li>
                                        <li><a href="#">Special 2</a></li>                   
                                    </ul>
                            </li>
                        </ul>              
                    </div>
                    </nav>  <!-- end .navbar --> 
                </div>  <!-- end container -->
            </div>  <!-- end mainNavigationRow -->
           
        </header>
        
        <!-- CAROUSEL -->
        <div class="container">
            <div class="row">
                <div id="workCarousel" class="carousel slide" data-ride="carousel">
                        
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                            
                        <!-- all other items go here -->
                        
                         <?php
                                for($i=0; $i<count($paintings);$i++) {
                                    
                                    if($i == 0) {
                                        echo '<div class="item active">'; //only the first item will be in the active item div
                                    }
                                        if($i!=0) {
                                            echo '<div class="item">'; //all other items will be plced in separate unactive item divs
                                        }
                                                echo '<div class="container">
                                                        <div class="col-md-10">
                                                            <h2>'.$paintings[$i][$title].'</h2>
                                                            <p><a href="#">'.$paintings[$i][$year].'</a></p>
                                                            <div class="row">
                                                
                                                                <div class="col-md-5">
                                                                    <img class="img-thumbnail img-responsive" src="art-images/paintings/medium/'.$paintings[$i][$ID].'.jpg" alt="'.$paintings[$i][$title].'" title="'.$paintings[$i][$title].'">
                                                                </div>
                                                                <div class="col-md-7">
                                                                    <p style="text-align: justify; 	text-justify: inter-word;">'; for($r=0;$r<500;$r++) { echo $paintings[$i][$description][$r]; } // the 5th column of the paintings array is a string of text. 
                                                                    //Since a sting is naturally an array I can use it to only display a certain amount of text.. currently it will only display the first 500 characters of the text
                                                                    echo '...</p> 
                                                                    <p class="price">'.$paintings[$i][$price].'</p>
                                                                    <div class="btn-group btn-group-lg">
                                                                        <button class="btn btn-default" type="button">
                                                                            <a href="#"><span class="glyphicon glyphicon-gift"></span> Add to Wish List</a>
                                                                        </button>
                                                                        <button class="btn btn-default" type="button">
                                                                            <a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Add to Shopping Cart</a>
                                                                        </button>
                                                                    </div>
                                                                    <p>&nbsp;</p>
                                                                    <div class="panel panel-default">
                                                                        <div class="panel-heading">Product Details</div>
                                                                        <table class="table">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <th>Date:</th>
                                                                                    <td>'.$paintings[$i][$year].'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>Medium:</th>
                                                                                    <td>'.$paintings[$i][$genre].'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>Dimensions:</th>
                                                                                    <td>'.$paintings[$i][$width].' x '.$paintings[$i][$height].'</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th>Home:</th>
                                                                                    <td><a href="#">'.$paintings[$i][$gallery].'</a></td>
                                                                                </tr>
                                                                                   <tr>
                                                                                    <th>Link:</th>
                                                                                    <td><a href="'.$paintings[$i][$URL].'">Wiki</a></td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div><!-- close panel panel-default -->
                                                                </div><!-- close col-md--7 -->
                                                            </div><!-- close row -->
                                                        </div><!-- close col-md-10 -->
                                                    </div><!-- close container -->
                                                </div><!-- close item -->';
                                            
                                }
                                            
                        ?>
                        
    
                    </div>  <!-- end carousel-inner -->
                                
                    <!-- Controls -->
                    <a class="left carousel-control" href="#workCarousel" role="button" data-slide="prev">
                      <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#workCarousel" role="button" data-slide="next">
                      <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
           
                </div><!-- Carousel --> 
            </div>
        </div>
                
            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
        
    </body>
</html>