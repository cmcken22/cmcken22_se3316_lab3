<html lang = en>
<head>
  
  <meta http-equiv="Content-Type" content="text/html;charset=us-ansi">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <title>Lab3 SE3316A</title>
    
  <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="carouselStyling.css" rel="stylesheet">
  
  <script language="JavaScript" type="text/javascript">
    $(document).ready(function(){
      $('.myCarousel').carousel({
          interval: 5000
      })
      });    
      </script> 
  <!-- <link rel="stylesheet" href="style.css"> -->
  
</head>
<body>
  
  <?php 
    
    $ID = 3;
    $title = 4;
    $description = 5;
    $year = 6;
    $width = 7; 
    $height = 8;
    $genre = 9;
    $gallery = 10;
    $price = 11;
    $URL = 12;
    
    $paintingsFile = file("data-files/paintings.txt");
    
    $paintings = array();
    
    foreach($paintingsFile as $data) {
      $info = explode("~", $data);
      $paintings[] = $info;
    }
    
    $firstLine = array();
    
    for($i=0;$i<count($paintings);$i++) {
      $string = $paintings[$i][5];
      $info = explode(".", $string);
      $firstLine[] = $info[0];
    }
    
    ?>
  

  <!-- NAVBAR -->

  <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  <!-- NAVBAR ENDS -->
    
  <!-- CAROUSEL -->
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
    </ol>
   
    <!-- Wrapper for slides -->
    <div class="carousel-inner">
      
      <?php 
      for($i=0;$i<5;$i++){
        if($i==0){
          echo '<div class="item active">'; //only the first item will be in the active item div
        }
        if($i!=0){
          echo '<div class="item">'; //all other items will be plced in separate unactive item divs
        }
        echo '<img rel="#PaintingThumb" src="art-images/paintings/medium/'.$paintings[$i][$ID].'.jpg" alt="..."></img> 
                <div class="carousel-caption">
                  <h1>'.$paintings[$i][$title].'</h1>
                    <p>'.$paintings[$i][$year].'</p>
                    <p><a class="btn btn-lg btn-primary" href="'.$paintings[$i][$URL].'" role="button">Learn More</a></p>
                </div>
              </div>';
      } ?>
    
    </div>
   
    <!-- Controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
    </a>
  </div> <!-- Carousel -->
  
  <div class="container marketing">
      <div class="row">
        
          <?php 
          
          for($i=5;$i<=10;$i++) {
            if($i%8==0){ //I want a new row to be created when i = 8
              echo '<div class="row">';
            }
            echo '<div class="col-lg-4">
                    <img class="img-circle" src="art-images/paintings/medium/'.$paintings[$i][$ID].'.jpg" 
                        alt='.$paintings[$i][$title].' title='.$paintings[$i][$title].' style="width:100px; height:100px;">
                    <h2>'.$paintings[$i][$title].'</h2>
                    <p class="text-justify">'.$firstLine[$i].'</p>
                    <p><a class="btn btn-default" href="'.$paintings[$i][$URL].'" role="button">View details</a></p>
                  </div>';
                  if($i%7==0){ //I need to close the first row when i = 7 before creating a new row at i = 8
                    echo '</div>';
                  }
          }
          ?>
          
          </div>
          
          
    </div><!-- close container marketing -->
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    
</body>
</html>