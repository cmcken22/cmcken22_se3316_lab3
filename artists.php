<html>
    <head>
        <meta charset="us-ansi">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Lab3 SE3316A</title>
    
        
        <link href="https://fonts.googleapis.com/css?family=Cuprum|Cookie" rel="stylesheet" type="text/css">  
        
        <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
        <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
        <script src="../../assets/js/ie-emulation-modes-warning.js"></script>
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <!-- Calling jquery first -->
        <script language="JavaScript" type="text/javascript" src="jquery.js"></script>
          
        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">
          
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        
        <!-- Optional theme -->
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        
        <!-- Latest compiled and minified JavaScript -->
        <script src="js/bootstrap.min.js"></script>
          
        <!-- Custom styles for this template -->
        <link href="Lab3Styles.css" rel="stylesheet">
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="../../assets/js/html5shiv.js"></script>
        <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
        
        <script language="JavaScript" type="text/javascript">
            $(document).ready(function(){
                $('.myCarousel').carousel({
                    interval: 5000
                })
            });    
            $(document).ready(function(){
                $('.myCarousel2').carousel({
                    interval:4500
                })
            });    
            $(document).ready(function(){
                $('.dropdown-toggle').dropdown()
            });
        </script>
    
    </head>
    <body>
    
    <?php 
    
    $ID = 0;
    $fName = 1;
    $lName = 2;
    $nationality = 3;
    $birthYear = 4; 
    $deathYear = 5;
    $description = 6;
    $URL = 7;
    
    $artistsFile = file("data-files/artists.txt");
    
    $artists = array();
    
    foreach($artistsFile as $data) {
      $info = explode("~", $data);
      $artists[] = $info;
    }
    
    ?>
        
        <header>
            <div id="topHeaderRow">
                <div class="container">
                    <nav class="navbar navbar-inverse " role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                        </div>
        
                        <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                            <ul class="nav navbar-nav">
                                <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                                <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                            </ul>
                        </div>  <!-- end .collpase --> 
                    </nav>  <!-- end .navbar --> 
                </div>  <!-- end .container --> 
            </div>  <!-- end #topHeaderRow --> 
           
            <div id="logoRow">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>Art Store</h1> 
                        </div>
                    
                    <div class="col-md-4">
                        <form class="form-inline" role="search">
                            <div class="input-group">
                                <label class="sr-only" for="search">Search</label>
                                <input class="form-control" placeholder="Search" name="search" type="text">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                                 </span>
                            </div>
                       </form> 
                    </div>   <!-- end .navbar --> 
                    </div>   <!-- end .row -->        
                </div>  <!-- end .container --> 
            </div>  <!-- end #logoRow --> 
           
            <div id="mainNavigationRow">
                <div class="container">
                    
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About Us</a></li>
                            <li><a href="work.php">Art Works</a></li>
                            <li class="active"><a href="artists.php">Artists</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Special 1</a></li>
                                        <li><a href="#">Special 2</a></li>                   
                                    </ul>
                            </li>
                        </ul>              
                    </div>
                    </nav>  <!-- end .navbar --> 
                </div>  <!-- end container -->
            </div>  <!-- end mainNavigationRow -->
        </header>
            
        <div class="container">
            <h2>This Week's Best Artists</h2>
            <div class="alert alert-warning" role="alert">Each week we show you who are our best artists ...</div>   
                <div class="row">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            
                        <?php
                        
                        echo '<div class="item active"><!-- item 0 -->
                                <div class="container"><!-- container 0 -->';
                                
                                for($i=0;$i<18;$i++) {
                                        if($i!=0 && $i%6 == 0){
                                            echo '<div class="item"><!-- item '.$i.' -->
                                                <div class="container"><!-- container '.$i.' -->';
                                        }
                                        echo '<div class="col-md-2">
                                        <div class="thumbnail">
                                            <img src="art-images/artists/medium/'.$artists[$i][$ID].'.jpg " style="width:175px; height:175px;"><br>
                                            <div class="caption">
                                                <h4>'.$artists[$i][$fName].' '.$artists[$i][$lName].'</h4>
                                                <p><a class="btn btn-info" role="button" href="'.$artists[$i][$URL].'">Learn more</a></p>
                                            </div><!-- close caption -->
                                            </div><!-- thumbnail -->
                                        </div><!-- close col-md-2 -->';
                                        if(($i+1)%6 == 0 && ($i+1)!=18){
                                                echo "</div><!-- close container '.$i.' -->
                                            </div><!-- close item '.$i.' -->";
                                        }
                                        
                                }
                                        echo '</div><!-- close container 0 -->
                                    </div><!-- close item active 0 -->';
                                    
                                    
                        
                        
                        
                        ?>
                        </div><!-- close carousel-inner -->
                        
                        <!-- Controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span></a>
                    </div><!-- close myCarousel -->
                </div><!-- close row -->
        
                <div class="row">
                    <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            
                        <?php
                        
                        echo '<div class="item active">
                                <div class="container">';
                                
                                for($i=20;$i<38;$i++) {
                                        if(($i-20)!=0 && ($i-20)%6 == 0){
                                            echo '<div class="item">
                                                <div class="container">';
                                        }
                                        echo '<div class="col-md-2">
                                        <div class="thumbnail">
                                            <img src="art-images/artists/medium/'.$artists[$i][$ID].'.jpg " style="width:175px; height:175px;"><br>
                                            <div class="caption">
                                                <h4>'.$artists[$i][$fName].' '.$artists[$i][$lName].'</h4>
                                                <p><a class="btn btn-info" role="button" href="'.$artists[$i][$URL].'">Learn more</a></p>
                                            </div><!-- close caption -->
                                            </div><!-- close thumbnail -->
                                        </div><!-- close col-md-2 -->';
                                        if(($i-19)%6 == 0 && ($i-19)!=18){
                                                echo '</div><!-- close container -->
                                            </div><!-- close item -->';
                                        }
                                        
                                }
                                        echo '</div><!-- close container -->
                                    </div><!-- close item active -->';
                                    
                                    
                        
                        
                        
                        ?>
                        </div><!-- close carousel-inner -->
                        
                        <!-- Controls -->
                        <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span></a>
                        <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span></a>
                        
                    </div><!-- close myCarousel2 -->
                </div>
                <h4>Artists by Genre</h4>
                <div class="progress">
                    <div class="progress-bar progress-bar-info" style="width: 7%">
                        <span>Gothic</span>
                    </div>
                    <div class="progress-bar progress-bar-success" style="width: 27%">
                        <span>Renaissance</span>
                    </div>
                    <div class="progress-bar progress-bar-warning" style="width: 15%">
                        <span>Baroque</span>
                    </div>
                    <div class="progress-bar progress-bar-danger" style="width: 21%">
                        <span >Pre-Modern</span>
                    </div>  
                    <div class="progress-bar" style="width: 30%">
                        <span >Modern</span>
                    </div>
                </div>
            </div>
        
        <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
            
    </body>
</html>